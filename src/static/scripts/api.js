// export const LANG = 'en_US';
export const LANG = 'pt-BR';

export const API_URL = 'http://api.themoviedb.org/3';
export const API_KEY = 'b25eed9bb6e478711c7647f5d238a1f9';
// Image Prefix
export const API_IMG_PREFIX = 'http://image.tmdb.org/t/p/w185_and_h278_bestv2';
// Discover
export const API_HIGHLIGHTS = `${API_URL}/discover/movie?api_key=${API_KEY}&language=${LANG}`;
// Genre
export const API_GENRES = `${API_URL}/genre/movie/list?api_key=${API_KEY}&language=${LANG}`;
export const API_GENRE = `${API_URL}/genre/28/movies?api_key=${API_KEY}&language=${LANG}`;
// Love Movies URLs
export const API_LM_URL_PREFIX = 'https://movies-love.herokuapp.com';
export const API_LM_WISHES = `${API_LM_URL_PREFIX}/wishes`;
export const API_LM_WISH = `${API_LM_URL_PREFIX}/wish`;
export const API_LM_WATCHEDS = `${API_LM_URL_PREFIX}/watcheds`;
export const API_LM_WATCH = `${API_LM_URL_PREFIX}/watch`;
export const API_LM_WAKE_UP = `${API_LM_URL_PREFIX}/live`;
export const API_MINIMUM_VOTE_COUNT = 10;
export const lockFeedback = false;
