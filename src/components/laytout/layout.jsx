import React, { Fragment } from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import '../../static/styles/global.scss';

// Component
const Layout = props => (
  <Fragment>
    <Head>
      <title>{props.title || 'Love Movies'}</title>
      <meta name="msapplication-tap-highlight" content="no" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta name="theme-color" content="#333" />
      <meta name="apple-mobile-web-app-title" content="Love Movies" />
      <meta name="apple-mobile-web-app-status-bar-style" content="default" />
      <meta name="apple-mobile-web-app-capable" content="yes" />
      <meta name="mobile-web-app-capable" content="yes" />
      <link rel="manifest" href="/static/manifest.webmanifest" />
      <link rel="shortcut icon" href="/static/images/icon.png" />
      <link rel="apple-touch-icon" href="/static/images/icon.png" />
      <link rel="stylesheet" href="/_next/static/style.css" />
    </Head>

    {props.children}
  </Fragment>
);

Layout.defaultProps = {
  title: ''
};

Layout.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node.isRequired
};

export default Layout;
