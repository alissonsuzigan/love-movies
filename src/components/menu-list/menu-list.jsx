import React from 'react';
import PropTypes from 'prop-types';

// Component
const MenuList = (props) => {
  const renderItems = item => (
    <li className="menu-item" key={item.id}>
      <h2 className="menu-title">{item.name}</h2>
    </li>
  );

  return (
    <ul className="menu-list">
      {props.data.map(renderItems)}
    </ul>
  );
};

MenuList.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape()).isRequired
};

export default MenuList;
