import React from 'react';
import Link from 'next/link';

// Component
const Tabs = () => (
  <section className="tabs">
    <nav className="tabs-nav">
      <Link prefetch href="/">Home</Link>
      <Link prefetch href="/genres">Categorias</Link>
    </nav>
  </section>
);

export default Tabs;
