import React from 'react';
import PropTypes from 'prop-types';

// Component
const Header = ({ title, leftButton, rightButton }) => (
  <header id="header" className="header">
    <div className="header-wrapper">
      <div className="header-left-area">
        {leftButton}
      </div>
      {title && <h1 className="header-title">{title}</h1>}
      <div className="header-right-area">
        {rightButton}
      </div>
    </div>
  </header>
);

Header.defaultProps = {
  title: '',
  leftButton: '',
  rightButton: ''
};

Header.propTypes = {
  title: PropTypes.string,
  leftButton: PropTypes.node,
  rightButton: PropTypes.node
};

export default Header;
