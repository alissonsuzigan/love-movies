import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';

// CSS Transition props
const classNames = {
  // appear: 'my-appear',
  // appearActive: 'my-active-appear',
  enter: 'drawer-enter',
  enterActive: 'drawer-enter-active',
  // enterDone: 'my-done-enter',
  exit: 'drawer-exit',
  exitActive: 'drawer-exit-active'
  // exitDone: 'my-done-exit',
};

// Component
class Drawer extends Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.isActive) document.body.classList.add('block-scroll');
    else document.body.classList.remove('block-scroll');
  }

  render() {
    const { handleMenu, isActive } = this.props;

    return (
      <CSSTransition in={isActive} timeout={700} classNames={classNames} unmountOnExit>
        <div className="drawer">
          <section className="drawer-content">
            <header className="drawer-header">
              <h1 className="drawer-test">
                Nome do Fulano
              </h1>
            </header>
            <ul>
              <li>drawer is loaded</li>
              <li>drawer is loaded</li>
              <li>drawer is loaded</li>
            </ul>
          </section>

          <div className="drawer-overlay" onMouseDown={handleMenu} role="presentation" />
        </div>
      </CSSTransition>
    );
  }
}

// Props validation
Drawer.propTypes = {
  isActive: PropTypes.bool.isRequired,
  handleMenu: PropTypes.func.isRequired
};

export default Drawer;
