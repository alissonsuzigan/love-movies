import React from 'react';
import PropTypes from 'prop-types';
// import ReleaseIcon from 'react-icons/lib/md/event';
import ReleaseIcon from 'react-icons/lib/md/today';
import AverageIcon from 'react-icons/lib/md/star';
import VoteIcon from 'react-icons/lib/md/group';
import WishIcon from 'react-icons/lib/md/add';
import WatchIcon from 'react-icons/lib/md/remove-red-eye';

// Static
import { API_IMG_PREFIX } from '../../static/scripts/api';

const MovieList = (props) => {
  const infoIconSize = 16;
  const actionIconSize = 28;

  const renderMovies = movie => (
    <li className="card-movie" key={movie.id}>
      <figure className="card-poster" >
        <img src={`${API_IMG_PREFIX}${movie.poster_path}`} alt={movie.title} />
      </figure>

      <div className="card-info">
        <h2 className="card-title">{movie.title}</h2>
        <p className="card-release">
          <ReleaseIcon size={infoIconSize} />
          <span>{movie.release_date}</span>
        </p>
        <p className="card-rating">
          <span className="card-average">
            <AverageIcon size={infoIconSize} className="star" />
            <span>{movie.vote_average}</span>
          </span>
          <span className="card-count">
            <VoteIcon size={infoIconSize} />
            <span>{movie.vote_count}</span>
          </span>
        </p>
      </div>

      <div className="card-action">
        <button><WishIcon size={actionIconSize} /></button>
        <button><WatchIcon size={actionIconSize} /></button>
      </div>
    </li>
  );

  return (
    <ul className="card-list">
      {props.data.results.map(renderMovies)}
    </ul>
  );
};

MovieList.propTypes = {
  data: PropTypes.shape().isRequired
};

export default MovieList;
