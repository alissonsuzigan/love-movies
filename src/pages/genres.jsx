import 'isomorphic-fetch';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IconMenu from 'react-icons/lib/md/menu';
import IconSearch from 'react-icons/lib/md/search';
// Components
import Layout from '../components/laytout';
import Header from '../components/header';
import MenuList from '../components/menu-list';
import Drawer from '../components/drawer';
import Tabs from '../components/tabs';
// Statics
import { API_GENRES } from '../static/scripts/api';

// Component
class Genres extends Component {
  static async getInitialProps() {
    const req = await fetch(API_GENRES);
    const { genres } = await req.json();
    return { genres };
  }

  constructor() {
    super();
    this.state = {
      showMenu: false
    };
    this.buttonClick = this.buttonClick.bind(this);
    this.handleMenu = this.handleMenu.bind(this);
  }

  buttonClick() {
    this.setState({ showMenu: !this.state.showMenu });
  }

  handleMenu() {
    this.setState({ showMenu: !this.state.showMenu });
  }

  render() {
    const leftButton = <button onClick={this.handleMenu}><IconMenu /></button>;
    const rightButton = <button onClick={this.buttonClick}><IconSearch /></button>;
    const { genres } = this.props;
    const { showMenu } = this.state;

    return (
      <Layout title="Home title">
        <Header
          title="Home"
          leftButton={leftButton}
          rightButton={rightButton}
        />
        <main id="main" className="main">
          {genres && <MenuList data={genres} />}
        </main>
        <Drawer isActive={showMenu} handleMenu={this.handleMenu} />
        <Tabs />
      </Layout>
    );
  }
}

Genres.propTypes = {
  genres: PropTypes.arrayOf(PropTypes.shape()).isRequired
};

export default Genres;
