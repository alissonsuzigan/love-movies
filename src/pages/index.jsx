import 'isomorphic-fetch';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IconMenu from 'react-icons/lib/md/menu';
import IconSearch from 'react-icons/lib/md/search';
// Components
import Layout from '../components/laytout';
import Header from '../components/header';
import CardMovie from '../components/card-movie';
import Drawer from '../components/drawer';
import Tabs from '../components/tabs';
// Statics
import { API_HIGHLIGHTS } from '../static/scripts/api';

// Component
class Home extends Component {
  static async getInitialProps() {
    const req = await fetch(API_HIGHLIGHTS);
    const movies = await req.json();
    return { movies };
  }

  constructor() {
    super();
    this.state = {
      showMenu: false
    };
    this.buttonClick = this.buttonClick.bind(this);
    this.handleMenu = this.handleMenu.bind(this);
  }

  buttonClick() {
    this.setState({ showMenu: !this.state.showMenu });
  }

  handleMenu() {
    this.setState({ showMenu: !this.state.showMenu });
  }

  render() {
    const leftButton = <button onClick={this.handleMenu}><IconMenu /></button>;
    const rightButton = <button onClick={this.buttonClick}><IconSearch /></button>;
    const { movies } = this.props;
    const { showMenu } = this.state;

    return (
      <Layout title="Home title">
        <Header
          title="Home"
          leftButton={leftButton}
          rightButton={rightButton}
        />
        <main id="main" className="main">
          {movies && <CardMovie data={movies} />}
        </main>
        <Drawer isActive={showMenu} handleMenu={this.handleMenu} />
        <Tabs />
      </Layout>
    );
  }
}

Home.propTypes = {
  movies: PropTypes.shape().isRequired
};

export default Home;
