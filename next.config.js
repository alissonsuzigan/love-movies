const path = require('path');
// Global paths
// const buildPath = path.resolve(process.cwd(), 'build');
const sourcePath = path.resolve(process.cwd(), 'src');
const configPath = path.resolve(process.cwd(), 'config');
// Loaders and plugins
const autoprefixer = require('autoprefixer');
const ExtractCSS = require('extract-text-webpack-plugin');


module.exports = {
  // distDir: 'build',
  webpack(config, options) {
    const { dev, isServer } = options;
    // config.output.path = buildPath;
    // console.log('=================== config:', config);

    config.resolve.extensions.push('.scss');
    config.module.rules.push(
      { // eslint config
        test: /\.jsx?$/,
        enforce: 'pre',
        include: sourcePath,
        loader: 'eslint-loader',
        options: {
          configFile: `${configPath}/.eslintrc`,
          emitWarning: dev
          // fix: true // autofix eslint errors
        }
      },
      { // scss config
        test: /\.scss$/,
        include: sourcePath,
        use: ExtractCSS.extract({
          use: [
            'css-loader',
            {
              loader: 'postcss-loader',
              options: {
                plugins: [autoprefixer]
              }
            },
            'sass-loader'
          ]
        })
      }
    );

    config.plugins.push(
      // Extract CSS
      new ExtractCSS({
        filename: 'static/style.css',
        // filename: 'static/[name].css',
        allChunks: true
      })
    );

    return config
  }
}
